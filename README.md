# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://zigakleine@bitbucket.org/zigakleine/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/zigakleine/stroboskop/commits/501a8fad399497e6dc8bdaf689aced517feaec05

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/zigakleine/stroboskop/commits/0c052f62d53e22d2b15329febda82535125a7037

Naloga 6.3.2:
https://bitbucket.org/zigakleine/stroboskop/commits/17487c56c23e1acb28c095b9c314aacb860154e3

Naloga 6.3.3:
https://bitbucket.org/zigakleine/stroboskop/commits/d7e7be0e234e6fa515f0bf19a4aa9e1da6f98ab0

Naloga 6.3.4:
https://bitbucket.org/zigakleine/stroboskop/commits/dd5daac25db18ae6ecacc15f282e12aba54e03d1?at=izgled

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/zigakleine/stroboskop/commits/c4963efa2996e96faf0003b5a442f386ac669a7a

Naloga 6.4.2:
https://bitbucket.org/zigakleine/stroboskop/commits/eb758cd6ff53247c3872f9f975a1bb0c0e6fa372

Naloga 6.4.3:
https://bitbucket.org/zigakleine/stroboskop/commits/4fcda7598f75f82a5a94f6d3ab6afc5791d624ea

Naloga 6.4.4:
https://bitbucket.org/zigakleine/stroboskop/commits/6c8069df2a3eadc5095526f49e0e246e798ff9a8